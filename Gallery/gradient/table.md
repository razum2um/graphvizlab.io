---
redirect_from:
  - /_pages/Gallery/gradient/table.html
layout: gallery
title: Table and Cell Gradients
svg: table.svg
gv_file: table.gv.txt
img_src: table.png
---
Demonstrates table and cell gradients.
