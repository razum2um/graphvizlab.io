---
redirect_from:
  - /_pages/Gallery/gradient/datastruct.html
layout: gallery
title: Gradients Applied to Data Struct Example
svg: datastruct.svg
gv_file: datastruct.gv.txt
img_src: datastruct.png
---
Demonstrates an application of gradients to record nodes.
