---
redirect_from:
  - /_pages/Gallery/gradient/linear_angle.html
layout: gallery
title: Gradient Linear Angles
svg: linear_angle.svg
gv_file: linear_angle.gv.txt
img_src: linear_angle.png
---
Demonstrates gradient linear angles.
