---
redirect_from:
  - /_pages/Gallery/gradient/colors.html
layout: gallery
title: Sample Gradient Color Schemes
svg: colors.svg
gv_file: colors.gv.txt
img_src: colors.png
---
Demonstrates the use of gradient colors.
