---
redirect_from:
  - /_pages/Gallery/gradient/cluster.html
layout: gallery
title: Cluster Gradients
svg: cluster.svg
gv_file: cluster.gv.txt
img_src: cluster.png
---
Demonstrates the use of cluster gradients.
