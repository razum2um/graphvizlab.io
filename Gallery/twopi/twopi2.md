---
redirect_from:
  - /_pages/Gallery/twopi/twopi2.html
layout: gallery
title: Radial Layout
svg: twopi2.svg
gv_file: twopi2.gv.txt
img_src: twopi2.png
---
A real-world network containing 300 sites over 40 countries.
The diagram was made to trace network incidents and to support maintenance.
Original names and other details were obfuscated for anonymity.
(This was not an AT&T network!)  Drawn using `twopi`.
