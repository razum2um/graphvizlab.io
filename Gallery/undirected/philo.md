---
redirect_from:
  - /_pages/Gallery/undirected/philo.html
layout: gallery
title: philo
svg: philo.svg
gv_file: philo.gv.txt
img_src: philo.png
---
"It encodes the so-called philosophers dilemma.  Neato pretty much approximates the way how humans would layout the graph." Contributed by Manfred Jeusfield.
