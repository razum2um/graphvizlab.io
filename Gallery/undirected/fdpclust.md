---
redirect_from:
  - /_pages/Gallery/undirected/fdpclust.html
layout: gallery
title: Undirected Graph Clusters
svg: fdpclust.svg
gv_file: fdpclust.gv.txt
img_src: fdpclust.png
---
The `fdp` layout program supports edges between nodes and clusters
and cluster-to-cluster.  The syntax is fairly obvious.  
`fdp` is being actively worked on, so the probability of hitting a
bug is higher than with `neato`. Also the quality of the layouts will
be improving. 
