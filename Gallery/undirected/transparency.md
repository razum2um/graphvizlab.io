---
redirect_from:
  - /_pages/Gallery/undirected/transparency.html
layout: gallery
title: Partially Transparent Colors
svg: transparency.svg
background: transparency.bg.png
copyright: Copyright &#169; 1996 AT&amp;T.  All rights reserved.
gv_file: transparency.gv.txt
img_src: transparency.png
---
This example illustrates the use of partially transparent colors for
node fill and graph background.
