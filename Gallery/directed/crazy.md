---
redirect_from:
  - /_pages/Gallery/directed/crazy.html
layout: gallery
title: Polygons
svg: crazy.svg
gv_file: crazy.gv.txt
img_src: crazy.png
---
A (non-authoritative) diagram of heritage of the Unix operating system.
This "crazy" version of the classic graph was created to stress test
the polygon shape generator and color map manager with random values.
