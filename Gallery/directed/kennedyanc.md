---
redirect_from:
  - /_pages/Gallery/directed/kennedyanc.html
layout: gallery
title: Family Tree
svg: kennedyanc.svg
gv_file: kennedyanc.gv.txt
img_src: kennedyanc.png
---
"I have implemented Genealogic descendant and ancestor graphs using Graphviz in FinFamily. ... I attach ... an ancestor graph from Caroline Bouvier Kennedy... " Contributed by Kaarle Kaila.

```
Blue box - man
Red ellipse - woman
Blue line - Father/Child relation
Red line - Mother/Child relation
Green line - Spouse relation
Orange line - Ancestors (other) children
Violet line - Ancestors (other) spouse 
```
