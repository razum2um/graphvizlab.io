---
redirect_from:
  - /_pages/Gallery/directed/fsm.html
layout: gallery
title: Finite Automaton
svg: fsm.svg
gv_file: fsm.gv.txt
img_src: fsm.png
---
This is a drawing of a finite automaton.
The `rankdir` and `orientation` request a
left-to-right drawing in landscape mode.
Note the use of text labels on edges.
