---
redirect_from:
  - /_pages/Gallery/directed/unix.html
layout: gallery
title: UNIX Family 'Tree'
svg: unix.svg
gv_file: unix.gv.txt
img_src: unix.png
---
This is our earliest example file.  The graph was originally
hand-drawn by Ian Darwin and Geoff Collyer in 1984 and 1986,
though we added to it a little.
