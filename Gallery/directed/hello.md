---
redirect_from:
  - /_pages/Gallery/directed/hello.html
layout: gallery
title: Hello World
svg: hello.svg
gv_file: hello.gv.txt
img_src: hello.png
---
A "Hello World" example made by giving the command:
```
echo "digraph G {Hello->;World}" | dot -Tpng >;hello.png
```
