---
redirect_from:
  - /_pages/Gallery/directed/world.html
layout: gallery
title: World Dynamics
svg: world.svg
gv_file: world.gv.txt
img_src: world.png
---
This is a graph from Forrester's book World Dynamics.
It originally appeared as an example in the Messinger,
Rowe et al paper on the GRAB system.  We added "same
rank" constraints to force a certain level assignment,
to evaluate edge crossing avoidance heuristics.
