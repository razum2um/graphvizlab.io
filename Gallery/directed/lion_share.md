---
redirect_from:
  - /_pages/Gallery/directed/lion_share.html
layout: gallery
title: lion_share
svg: lion_share.svg
gv_file: lion_share.gv.txt
img_src: lion_share.png
---
"A few people in the field of genetics are using dot to draw "marriage node diagram"  pedigree drawings.  Here is one I have done of a test pedigree from the FTREE pedigree drawing package (Lion Share was a racehorse)." Contributed by David Duffy.
