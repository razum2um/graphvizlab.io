---
redirect_from:
  - /_pages/Gallery/directed/switch.html
layout: gallery
title: Switch Network
svg: switch.svg
gv_file: switch.gv.txt
img_src: switch.png
---
A non-blocking switch network consisting of only 2-way splitters and selectors.
This graph was created to illustrate the parallel edge feature.
